package Main;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.CyclicBarrier;

import LTS.State;
import LTS.Transition;

public class TransContainer {
	private LinkedList<State> todo = new LinkedList<State>();
	private HashMap<Transition,Boolean> trans = new HashMap<Transition,Boolean>();
	private boolean finished = false;
	private CyclicBarrier barrier = new CyclicBarrier((Main.NUMBER_OF_AGENT), new Runnable() {
		
		@Override
		public void run() {
			setFinish(true);		
		}
	});
	
    private CyclicBarrier barrier2 = new CyclicBarrier(Main.NUMBER_OF_AGENT, new Runnable() {
		
		@Override
		public void run() {
			note();
			
		}
	});
	
    //_______________________CONSTRUCTOR_____________________
	public TransContainer() {
		todo.addAll(Main.stateSet.values());
		for (Transition t : Main.transitionSet) {
			trans.put(t,t.getWeak());
		}
	}
	
    //____________________GETTER AND SETTER__________________
	public CyclicBarrier getBarrier() {
		return barrier;
	}
	
    public CyclicBarrier getBarrier2() {
    	return barrier2;
    }
    
	public synchronized boolean finished() {
		return finished;
	}
	
	public synchronized void setFinish(boolean b) {
		finished = b;
	}
	
	public synchronized HashSet<Transition> getTrans() {
		HashSet<Transition> res = new HashSet<Transition>(trans.keySet());
		return res;
	}
	
	public synchronized HashMap<Transition,Boolean> getMap() {
		return trans;
	}
	
	
	//____________________________METHODS_____________________
	public void startWeak() {
    	for (int i = 0; i < Main.NUMBER_OF_AGENT; i++) {
    		TransExecute agent = new TransExecute(this);
    		agent.start();
    	}
	}
	
	/**
	 * only used in Barrier runnable.
	 */
    public synchronized void note() {
    	notify();
    }
    
	public synchronized void addTrans(HashSet<Transition> set, State s,boolean found) {
		for (Transition t : set) {
			trans.put(t, t.getWeak());
		}
		if (found) {
			todo.addLast(s);
		}
		barrier.reset();
	}

	public synchronized State getWork() {
		if (!todo.isEmpty()) {
    		return todo.pop();
    	} else {
    		return null;
    	} 
	}
	
}
