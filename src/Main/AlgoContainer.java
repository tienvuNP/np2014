package Main;


import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.CyclicBarrier;

import LTS.State;
import LTS.Transition;
/**
 * holds synchronized data for minimizing algorithm. 
 */
public class AlgoContainer{
    private HashSet<Block> blocks = new HashSet<Block>();
    private LinkedList<ToDoEntry> todo = new LinkedList<ToDoEntry>();
    private boolean finished = false;
    private HashSet<Block> removed = new HashSet<Block>();
    private HashMap<Pair,HashSet<State>> preSet = new HashMap<Pair, HashSet<State>>();
    private CyclicBarrier barrier = new CyclicBarrier((Main.NUMBER_OF_AGENT), new Runnable() {
		
		@Override
		public void run() {
			setFinished(true);		
		}
	});
    private CyclicBarrier barrier2 = new CyclicBarrier(Main.NUMBER_OF_AGENT, new Runnable() {
		
		@Override
		public void run() {
			note();
			
		}
	});

    //_______________________CONSTRUCTOR_____________________
    public AlgoContainer(Collection<State> collection) {
    	blocks.add(new Block(collection));
    	todo.add(new ToDoEntry(new Block(collection), new Block(collection)));
		for (Transition t : Main.transitionSet) {
			Pair temp = new Pair(t.getTarget(), t.getLabel());
			if (!preSet.containsKey(temp)) {
				preSet.put(temp, new HashSet<State>());	
			}
			preSet.get(temp).add(t.getState());
		}
    }
    
    //____________________GETTER AND SETTER__________________
    public synchronized HashSet<Block> getBlocks() {
    	return blocks;
    }
    
    public synchronized void setFinished(boolean b) {
    	finished = b;
    }
    /**
     * Not synchronized because only reading access.
     * @param s
     * @return
     */
    public HashSet<State> getPre(Pair s) {
    	return preSet.get(s);
    }
    
    public CyclicBarrier getBarrier() {
    	return barrier;
    }
    
    public CyclicBarrier getBarrier2() {
    	return barrier2;
    }
    
    public synchronized boolean finished() {
    	return finished;
    }
    
	//____________________________METHODS_____________________
    public synchronized void note() {
    	notify();
    }
    
    public synchronized void modifyBlocks(Block remove, Block add1, Block add2, ToDoEntry todoE) {
    	if(removed.contains(remove) || removed.contains(todoE.getBlock1()) || removed.contains(todoE.getBlock2())) {
    		return;
    	}
    	if(remove !=null) {
    		blocks.remove(remove);
    		removed.add(remove);
    	}
    	for (Block b : blocks) {
    		todo.add(new ToDoEntry(add1, b));
    		todo.add(new ToDoEntry(b, add1));
    		todo.add(new ToDoEntry(add2, b));
    		todo.add(new ToDoEntry(b, add2));
    		
    	}
    	blocks.add(add1);
    	blocks.add(add2);
		todo.add(new ToDoEntry(add1, add2));
		todo.add(new ToDoEntry(add2, add1));
		todo.add(new ToDoEntry(add1, add1));
		todo.add(new ToDoEntry(add2, add2));
		barrier.reset();
    }
    
    public void startMin(int n) {
    	for (int i = 0; i < n; i++) {
    		AlgoExecute agent = new AlgoExecute(this);
    		agent.start();
    	}
    }
    
    public synchronized ToDoEntry getWork() {
		if (!todo.isEmpty()) {
			while (removed.contains(todo.getFirst().getBlock1()) || removed.contains(todo.getFirst().getBlock2())) {
				todo.remove();
			}
    		return todo.pop();
    	} else {
    		return null;
    	} 	
    }
}
