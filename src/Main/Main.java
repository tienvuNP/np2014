package Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import LTS.State;
import LTS.Transition;
import Utility.PseuCoShare;

public class Main {
    
	public final static int NUMBER_OF_AGENT =8;
	public static HashSet<Transition> oldTrans;
	public static HashMap<String,State> stateSet;
    public static HashSet<Transition> transitionSet;
    public static HashSet<String> labelSet;    

		/**
	 * Demonstrates how to open a LTS in pseuCo.com
	 * 
	 * @param data
	 * 		The LTS as a JSON object.
	 */
	public static void openInBrowserDemo(JsonObject data) {
		PseuCoShare sharer = new PseuCoShare();
		try {
			sharer.submitAndOpenLts(data);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	/**
	 * Reads the standard input.
	 * 
	 * @return The text entered on standard input, without newlines.
	 * @throws IOException
	 */
	public static String readStandardInput() throws IOException {
	    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	    
	    StringBuilder builder = new StringBuilder();
	    
	    String s;
	    while ((s = in.readLine()) != null && s.length() != 0) { // read until end or empty line
	    	// reading this way strips empty lines, but they are not needed anyway
	    	builder.append(s);
	    }
	
		return builder.toString(); 
	}
	/* __________________________________________________________________________________________
	 * __________________________________________________________________________________________
	 * __________________________________________________________________________________________
	*/
	
	public static void main(String[] args) throws IOException {
            if (args.length == 1 && args[0].equals("-i")) {
                // started with command line argument -i
                    
                // read the input
            	HashSet<Transition> temp1 = new HashSet<Transition>();
            	HashMap<String,State> temp2 = new HashMap<String, State>();
                HashSet<Transition> temp3 = new HashSet<Transition>();
                HashSet<String> temp4 = new HashSet<String>();
            	stateSet=temp2;
            	transitionSet=temp3;
            	labelSet=temp4;
            	oldTrans=temp1;
                String input = readStandardInput();
                parse(input);
                reach();
                calcWeak();
                minimize();
                String output = parseToJSON().toString();
                // output the result on standard output
                System.out.println(output);
            }
	}
	/**
	 * Adds all weak Transition to current set of transition. Furthermore saves those transitions in oldTrans for later use.
	 */
	public static void calcWeak() {
		//saving initial tau transition for later use
		for (Transition t : transitionSet) {
			if (t.getState().getIni() && t.getLabel().equals("τ")) {
				oldTrans.add(new Transition(t.getState(), t.getLabel(), t.getDetailsLabel(), t.getTarget()));
			}
		}
		//adding tau ohren
		for (State s : stateSet.values()) {
			transitionSet.add(new Transition(s, "τ", null, s, true));		
		}
		//concurrent part
		WeakContainer wc = new WeakContainer();
		wc.startWeak();
		synchronized(wc) {
			try {
				wc.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//concurrent part end
		transitionSet.clear();
		transitionSet.addAll(wc.getTrans());
	}
	
	/**
	 * Removes all unreachable states with its transitions.
	 */
	public static void reach() {
		//find intial state
		State init = null;
		for (State s : stateSet.values()) {
			if (s.getIni()) {
				init = s;
				break;
			}
		}
		//keep expanding post from initial state until no more states are found. result is saved in HashSet<State> calc. 
		HashSet<State> calc = new HashSet<State>();
		int size = 0;
		calc.add(init);
		while (size != calc.size()) {
			size = calc.size();
			HashSet<State> temp = new HashSet<State>();
			for (State s2 : calc) {
				temp.addAll(s2.post());
			}
			calc.addAll(temp);
		}
		//remove unused transition
		HashSet<Transition> rem = new HashSet<Transition>();
		for (Transition t : transitionSet) {
			if (!(calc.contains(t.getState()) && calc.contains(t.getTarget()))) {
				rem.add(t);
			} 			
		}
		transitionSet.removeAll(rem);
		//remove all unreachable states
		HashSet<State> rem2 = new HashSet<State>();
		for (State s : stateSet.values()) {
			if (!calc.contains(s)) {
				rem2.add(s);
			}
		}
		for (State s : rem2) {
			stateSet.remove(s.getName());
		}
	}
	/**
	 * Last part of algorithm to minimize LTS.
	 */
	public static void minimize() {
		//concurrent part
		AlgoContainer ac = new AlgoContainer(stateSet.values());
		ac.startMin(NUMBER_OF_AGENT);
		synchronized(ac) {
			try {
				ac.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//end of concurrent part	
		//number of states is minimized now. building of new minimized LTS starts here
		HashMap<State,Block> stateMap = new HashMap<State, Block>();
		HashMap<Block,String> blockMap = new HashMap<Block,String>();
		stateSet.clear();
		int i = 0;
		for (Block b : ac.getBlocks()) {
			boolean init = false;
			blockMap.put(b, ""+i);
			for (State s : b) {
				stateMap.put(s, b);
				if(s.getIni()) {
					init = true;
				}
			}
			stateSet.put(""+i, new State(""+i,init));
			i++;
			
		}
		//no tau Ohren
		HashSet<Transition> newT = new HashSet<Transition>();
		for (Transition t : transitionSet) {
			t.setState(stateSet.get(blockMap.get(stateMap.get(t.getState()))));
			t.setTarget(stateSet.get(blockMap.get(stateMap.get(t.getTarget()))));
			t.setWeak(false);
			if (!(t.getState().equals(t.getTarget()) && t.getLabel().equals("τ"))) {
				newT.add(t);
			} 
		}
		transitionSet = newT;
		//transitive Reduktion
		TransContainer tc = new TransContainer();
		tc.startWeak();
		synchronized(tc) {
			try {
				tc.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		transitionSet.clear();
		for (Transition t : tc.getTrans()) {
			if(!tc.getMap().get(t)) {
				transitionSet.add(t);
			}
		}
		//initial tau Ohr
		for (Transition t : oldTrans) {
			if (stateMap.get(t.getState()).equals(stateMap.get(t.getTarget()))) {
				transitionSet.add(new Transition(stateSet.get(blockMap.get(stateMap.get(t.getState()))), "τ", t.getDetailsLabel(), stateSet.get(blockMap.get(stateMap.get(t.getTarget())))));
			}
		}
	}
	

	/**
	 * parse String into our LTS model. Input String has to be correct.
	 * @param s
	 */
	public static void parse(String s) {
            JsonObject ltsObject = Json.createReader(new StringReader(s)).readObject();
            String initialStateLabel = ltsObject.getString("initialState");

            //adding initial State
            JsonObject statesObject = ltsObject.getJsonObject("states");
            for (String state : statesObject.keySet()) {
                JsonObject stateObject = statesObject.getJsonObject(state);

                //adding State
                State newS;
                if (state.equals(initialStateLabel)) {
                	 newS = new State(state,true);
                } else {
                	newS = new State(state);
                }
               
                stateSet.put(state, newS);
                JsonArray transitionsArray = stateObject.getJsonArray("transitions");
                for (int i = 0; i < transitionsArray.size(); i++) {
                    JsonObject transition = transitionsArray.getJsonObject(i);
                    String label = transition.getString("label");
                    //adding Label
                    if (labelSet.contains(label)) {
                    	for (String l2 : labelSet) {
                    		if (l2.equals(label)) {
                    			label = l2;
                    		}
                    	}
                    } else {
                    	labelSet.add(label);
                    }
                    String detailsLabel = null;
                    try {
                        detailsLabel = transition.getString("detailsLabel");
                        } catch (ClassCastException e) {
                        // ignore - detailsLabel = null
                        } catch (NullPointerException e) {
                        // ignore
                    }

                    String target = transition.getString("target");
                    //adding Transition
                    State newTS = new State(target);
                    if (stateSet.containsKey(newTS.getName())) {
                    	transitionSet.add(new Transition(newS, label, detailsLabel, stateSet.get(newTS.getName())));
                    } else {
                    	transitionSet.add(new Transition(newS, label, detailsLabel, newTS));
                    }                  
                }
            }
	}
	
	/**
	 * parse from current LTS model state to JSON Object.
	 * @return
	 */
	public static JsonObject parseToJSON() {
		State ini = null;
		JsonObjectBuilder builder = Json.createObjectBuilder();
		for (State s : stateSet.values()) {		
			if (s.getIni()) {
				ini = s;
			}
			JsonArrayBuilder trans = Json.createArrayBuilder();
			for (Transition t: transitionSet) {
				if(t.getState().equals(s)) {
					if (t.getDetailsLabel()!=null) {
						trans.add(Json.createObjectBuilder().add("label", t.getLabel()).add("detailsLabel", t.getDetailsLabel()).add("target", t.getTarget().getName()).build());
					} else {
						trans.add(Json.createObjectBuilder().add("label", t.getLabel()).add("detailsLabel", false).add("target", t.getTarget().getName()).build());
					}
				}
			}
			builder.add(s.getName(), Json.createObjectBuilder().add("transitions", trans.build()).build());	
		}
		JsonObject ltsObject = Json.createObjectBuilder()
				.add("initialState", ini.getName())
				.add("states", builder.build())
				.build();
		return ltsObject;
	}
}
