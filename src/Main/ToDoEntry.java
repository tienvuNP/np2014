package Main;

/**
 * represents an entry in the todo list. the todo list ist used in AlgoContainer to manage the running Threads.
 *
 */
public class ToDoEntry {
	private Block block1;
	private Block block2;
	
	public ToDoEntry(Block block1, Block block2) {
		this.block1 = block1;
		this.block2 = block2;
	}
	public Block getBlock1() {
		return block1;
	}
	public Block getBlock2() {
		return block2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((block1 == null) ? 0 : block1.hashCode());
		result = prime * result + ((block2 == null) ? 0 : block2.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ToDoEntry other = (ToDoEntry) obj;
		if (block1 == null) {
			if (other.block1 != null)
				return false;
		} else if (!block1.equals(other.block1))
			return false;
		if (block2 == null) {
			if (other.block2 != null)
				return false;
		} else if (!block2.equals(other.block2))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Work with " + block1 + " and " + block2;
	}
	
	
	
}
