package Main;

import java.util.HashSet;
import java.util.concurrent.BrokenBarrierException;

/**
 * Threads for minimizing algorithm. They work on synchronized data in AlgoContainer.
 *
 */
public class AlgoExecute extends Thread{

	private AlgoContainer cont;
	
	public AlgoExecute(AlgoContainer c) {
		cont = c;
	}
	
	@Override
	public void run() {
		while (!cont.finished()) {
			ToDoEntry todo = cont.getWork();
			if (todo==null) {
				try {
					cont.getBarrier().await();
					cont.getBarrier2().await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (BrokenBarrierException e) {
					continue;
				}
				continue;
			}
			for (String l : Main.labelSet) {
				Block b1 = todo.getBlock1().getCopySet();
				Block b2 = todo.getBlock1().getCopySet();
				HashSet<LTS.State> pre = new HashSet<LTS.State>();
				for (LTS.State s : todo.getBlock2()) {
					HashSet<LTS.State> temp = cont.getPre(new Pair(s,l));
					if (temp!=null) {
						pre.addAll(cont.getPre(new Pair(s,l)));
					}
				}
				b1.retainAll(pre);
				b2.removeAll(pre);
				if (!b1.isEmpty() && !b2.isEmpty()) {
					Block temp1 = new Block(b1);
					Block temp2 = new Block(b2);
					cont.modifyBlocks(todo.getBlock1(), temp1, temp2, todo);
					break;
				}
				
			}
		}
	}

}
