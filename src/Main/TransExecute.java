package Main;

import java.util.HashSet;
import java.util.concurrent.BrokenBarrierException;

import LTS.Transition;

/**
 * Thread to calculate transitive reduction.
 *
 */
public class TransExecute extends Thread{
	
	private TransContainer tc;
	
	public TransExecute(TransContainer w) {
		tc = w;
	}

	@Override
	public void run() {
		while (!tc.finished()) {
			HashSet<Transition> newTrans = new HashSet<Transition>();
			HashSet<Transition> current = tc.getTrans();
			boolean found = false;
			LTS.State s = tc.getWork();
			if (s!=null) {
				for (Transition t : current) {
					if (t.getState().equals(s)) {
						if(t.getLabel().equals("τ")) {
							LTS.State mid = t.getTarget();
							for (Transition t2 : current) {
								if (t2.getState().equals(mid)) {
									Transition newT = new Transition(s, t2.getLabel(), t2.getDetailsLabel(), t2.getTarget(), true);
									if (!current.contains(newT)) {
										found = true;
									}
									newTrans.add(newT);
								}
							}
						} else {
							LTS.State mid = t.getTarget();
							for (Transition t2 : current) {
								if (t2.getState().equals(mid) && t2.getLabel().equals("τ")) {
									Transition newT = new Transition(s, t.getLabel(), t.getDetailsLabel(), t2.getTarget(), true);
									if (!current.contains(newT)) {
										found = true;
									}
									newTrans.add(newT);
								}
							}
						}
					}
				}
				tc.addTrans(newTrans, s,found);

				
			} else {
				try {
					tc.getBarrier().await();
					tc.getBarrier2().await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (BrokenBarrierException e) {
					continue;
				}	
			}
		
		}
	}

}
