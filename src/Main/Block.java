package Main;

import java.util.Collection;
import java.util.HashSet;

import LTS.State;
/**
 * represents a set of states (therefore extending from HashSet<Transition>). used for minimizing algorithm.
 *
 */
public class Block extends HashSet<State>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8472718374103522915L;
	
	public Block(Collection<State> collection) {
		super(collection);
	}
	public Block() {
		super();
	}
	
	public Block getCopySet() {
		return (Block) this.clone();
	}
	/**
	 * calculates pre of every state in this set. returns everything in one set.	
	 * @param label
	 * @return
	 */
	public Block pre(String label) {
		Block result = new Block();
		for (State s: this) {
			result.addAll(s.pre(label));
		}
		return new Block(result);
	}
	
	@Override
	public String toString() {
		String res = "{";
		int i = 0;
		for (State s : this) {
			if (i<this.size()-1) {
				res = res + s.toString()+ ", ";
			} else {
				res = res + s.toString();
			}
			i++;
		}
		res += "}";
		return res;
	}

}
