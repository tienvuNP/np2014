package Main;

import java.util.HashSet;
import java.util.concurrent.BrokenBarrierException;

import LTS.Transition;

/**
 * Thread to calculate weak transitions.
 *
 */
public class WeakExecute extends Thread{
	
	private WeakContainer wc;
	
	public WeakExecute(WeakContainer w) {
		wc = w;
	}

	@Override
	public void run() {
		while (!wc.finished()) {
			HashSet<Transition> newTrans = new HashSet<Transition>();
			HashSet<Transition> current = wc.getTrans();
			boolean found = false;
			LTS.State s = wc.getWork();
			if (s!=null) {
				for (Transition t : wc.getTransition(s)) {
					HashSet<Transition> temp = wc.getTransition(t.getTarget());
					for (Transition t2 : temp) {
						if (t2.getLabel().equals("τ")) {
							Transition newT = new Transition(s, t.getLabel(), t.getDetailsLabel(), t2.getTarget(), true);
							if (!current.contains(newT)) {
								found = true;
							}
							newTrans.add(newT);
						} else {
							if (t.getLabel().equals("τ") && !t2.getLabel().equals("τ")) {
								Transition newT = new Transition(s, t2.getLabel(), t2.getDetailsLabel(), t2.getTarget(), true);
								if (!current.contains(newT)) {
									found = true;
								}
								newTrans.add(newT);
							}
						}
					}
				}
				wc.addTrans(newTrans, s,found);
			} else {
				try {
					wc.getBarrier().await();
					wc.getBarrier2().await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (BrokenBarrierException e) {
					continue;
				}	
			}
		
		}
	}

}
