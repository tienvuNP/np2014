package Main;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.CyclicBarrier;

import LTS.State;
import LTS.Transition;
/**
 * holds synchronized data for the calculation of weak transition. similar structured to AlgoContainer.
 *
 */
public class WeakContainer {
	private LinkedList<State> todo = new LinkedList<State>();
	private HashSet<Transition> trans = new HashSet<Transition>();
	private HashMap<State,HashSet<Transition>> stateTrans = new HashMap<State, HashSet<Transition>>();
	private boolean finished = false;
	private CyclicBarrier barrier = new CyclicBarrier((Main.NUMBER_OF_AGENT), new Runnable() {
		
		@Override
		public void run() {
			setFinish(true);		
		}
	});
	
    private CyclicBarrier barrier2 = new CyclicBarrier(Main.NUMBER_OF_AGENT, new Runnable() {
		
		@Override
		public void run() {
			note();
			
		}
	});
	
    //_______________________CONSTRUCTOR_____________________
	public WeakContainer() {
		todo.addAll(Main.stateSet.values());
		for (State s : Main.stateSet.values()) {
			stateTrans.put(s, new HashSet<Transition>());
		}
		for (Transition t : Main.transitionSet) {
			stateTrans.get(t.getState()).add(t);
		}
	}
	
    //____________________GETTER AND SETTER__________________
	public CyclicBarrier getBarrier() {
		return barrier;
	}
	
	public synchronized HashSet<Transition> getTransition(State s) {
		HashSet<Transition> res = new HashSet<Transition>();
		res.addAll(stateTrans.get(s));
		return res;
	}
    public CyclicBarrier getBarrier2() {
    	return barrier2;
    }
    
	public synchronized boolean finished() {
		return finished;
	}
	
	public synchronized void setFinish(boolean b) {
		finished = b;
	}
	
	public synchronized HashSet<Transition> getTrans() {
		HashSet<Transition> res = new HashSet<Transition>(trans);
		return res;
	}
		
	//____________________________METHODS_____________________
	public void startWeak() {
    	for (int i = 0; i < Main.NUMBER_OF_AGENT; i++) {
    		WeakExecute agent = new WeakExecute(this);
    		agent.start();
    	}
	}
	
    public synchronized void note() {
    	notify();
    }
    
	public synchronized void addTrans(HashSet<Transition> set, State s,boolean found) {
		trans.addAll(set);
		stateTrans.get(s).addAll(set);
		if (found) {
			todo.addLast(s);
		}
		barrier.reset();
	}

	public synchronized State getWork() {
		if (!todo.isEmpty()) {
    		return todo.pop();
    	} else {
    		return null;
    	} 
	}
	
}
