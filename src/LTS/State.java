package LTS;

import java.util.HashSet;

import Main.Main;

/**
 * Represents a state of LTS.
 *
 */
public class State {
    private String name;
    private boolean initial = false;
    
  //_______________________CONSTRUCTOR_____________________
    public State(String s) {
        name = s;
    }
    public State(String s, boolean b) {
    	initial = b;
        name = s;
    }
  //____________________GETTER AND SETTER__________________
    public boolean getIni() {
    	return initial;
    }
    public String getName() {
    	return name;
    }
    public void setName(String s) {
    	name = s;
    }
  //____________________________METHODS_____________________
    /**
     * Finds all pre of this state.
     * @return
     */
    public HashSet<State> pre(){
    	HashSet<State> res  = new HashSet<State>();
    	for (Transition t : Main.transitionSet) {
    		if (t.getTarget().equals(this)) {
    			res.add(t.getState());
    		}
    	}
    	return res;
    }
    /**Finds all states which have a transition with label l to this state.
     * @param l
     * @return
     */
    public HashSet<State> pre(String l){
    	HashSet<State> res  = new HashSet<State>();
    	for (Transition t : Main.transitionSet) {
    		if (t.getTarget().equals(this) && t.getLabel().equals(l)) {
    			res.add(t.getState());
    		}
    	}
    	return res;
    }
    /**returns post of this state
     * @return
     */
    public HashSet<State> post(){
    	HashSet<State> res  = new HashSet<State>();
    	for (Transition t : Main.transitionSet) {
    		if (t.getState().equals(this)) {
    			res.add(t.getTarget());
    		}
    	}
    	return res;
    }
    @Override
    public String toString() {
        return "[" + name + "]";
    }
    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
}
