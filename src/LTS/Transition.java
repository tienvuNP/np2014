package LTS;
/**
 * Represents transition in our LTS. Has no further methods besides getter and setter.

 *
 */
public class Transition {

    private State state;
    private String label;
    private String detailsLabel;
    private State target;
    private boolean weak;
    
  //_______________________CONSTRUCTOR_____________________
       public Transition(State state2, String label2, String detailsLabel2, State target2) {
    	    weak = false;
	        state = state2;
	        label = label2;
	        detailsLabel = detailsLabel2;
	        target = target2;
       }
       public Transition(State state2, String label2, String detailsLabel2, State target2, boolean w) {
   	    	weak = w;
	        state = state2;
	        label = label2;
	        detailsLabel = detailsLabel2;
	        target = target2;
      }
     //____________________GETTER AND SETTER__________________
       public State getState() {
		return state;
	}

    public boolean getWeak() {
    	return weak;
    }
    
    public void setWeak(boolean b) {
    	weak = b;
    }
       
	public String getLabel() {
		return label;
	}

	public String getDetailsLabel() {
		return detailsLabel;
	}

	public State getTarget() {
		return target;
	}

	public void setState(State s) {
		state = s;
	}
	
	public void setTarget(State s) {
		target = s;
	}
	@Override
       public String toString() {
           return "[Transition: "
                   + "State: " + state + ", "
                   + "Label: " + label + ", "
                   + "Weak: " + weak + ", "
                   + "Target: " + target + "]";
       }
       
       @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transition other = (Transition) obj;
		if (label == null) {
			if (other.label != null)
				return false;
		} else if (!label.equals(other.label))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}
       
       @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}
}
