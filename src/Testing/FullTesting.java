package Testing;

import isomorph.IsoChecker;

import java.io.IOException;
import java.lang.annotation.Repeatable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import LTS.State;
import LTS.Transition;
import Main.Main;

public class FullTesting {

	@Before
	public void setUp() {
    	HashSet<Transition> temp1 = new HashSet<Transition>();
    	HashMap<String,State> temp2 = new HashMap<String, State>();
        HashSet<Transition> temp3 = new HashSet<Transition>();
        HashSet<String> temp4 = new HashSet<String>();
    	Main.stateSet=temp2;
    	Main.transitionSet=temp3;
    	Main.labelSet=temp4;
    	Main.oldTrans=temp1;
	}
	
	@Test
	public void test1() {
		Main.labelSet.clear();
		Main.stateSet.clear();
		Main.transitionSet.clear();
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/testcase1.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/testcase1.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	
	@Test
	public void test2() {
		Main.labelSet.clear();
		Main.stateSet.clear();
		Main.transitionSet.clear();
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/testcase2.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/testcase2.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	
	@Test
	public void test3() {
		Main.labelSet.clear();
		Main.stateSet.clear();
		Main.transitionSet.clear();
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/testcase3.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/testcase3.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	
	@Test
	public void test4() {
		Main.labelSet.clear();
		Main.stateSet.clear();
		Main.transitionSet.clear();
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/testcase4.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/testcase4.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	
	@Test
	public void test5() {
		Main.labelSet.clear();
		Main.stateSet.clear();
		Main.transitionSet.clear();
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/testcase5.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/testcase5.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	
	@Test
	public void test6() {
		Main.labelSet.clear();
		Main.stateSet.clear();
		Main.transitionSet.clear();
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/testcase6.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/testcase6.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}

}
