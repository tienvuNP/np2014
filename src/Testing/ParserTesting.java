package Testing;
import java.util.HashMap;
import java.util.HashSet;

import isomorph.IsoChecker;

import javax.json.Json;
import javax.json.JsonObject;

import org.junit.Before;
import org.junit.Test;

import LTS.State;
import LTS.Transition;
import Main.Main;


public class ParserTesting {
	
	@Before
	public void setUp() {
    	HashSet<Transition> temp1 = new HashSet<Transition>();
    	HashMap<String,State> temp2 = new HashMap<String, State>();
        HashSet<Transition> temp3 = new HashSet<Transition>();
        HashSet<String> temp4 = new HashSet<String>();
    	Main.stateSet=temp2;
    	Main.transitionSet=temp3;
    	Main.labelSet=temp4;
    	Main.oldTrans=temp1;
	}

	@Test
	public void test() {
		JsonObject statesObject = Json.createObjectBuilder()
				.add("A", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.add(Json.createObjectBuilder().add("label", "τ").add("detailsLabel", false).add("target", "B").build())
						.build()).build())
				.add("B", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.add(Json.createObjectBuilder().add("label", "τ").add("detailsLabel", false).add("target", "C").build())
						.add(Json.createObjectBuilder().add("label", "b").add("detailsLabel", false).add("target", "C").build())
						.build()).build())
				.add("C", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.add(Json.createObjectBuilder().add("label", "τ").add("detailsLabel", false).add("target", "D").build())
						.add(Json.createObjectBuilder().add("label", "a").add("detailsLabel", false).add("target", "D").build())
						.build()).build())
				.add("D", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.add(Json.createObjectBuilder().add("label", "τ").add("detailsLabel", false).add("target", "E").build())
						.build()).build())
				.add("E", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.build()).build())
				.build();
		JsonObject ltsObject = Json.createObjectBuilder()
				.add("initialState", "A")
				.add("states", statesObject)
				.build();
		Main.parse(ltsObject.toString());
		IsoChecker.assertIsomorphic(ltsObject.toString(), Main.parseToJSON().toString());
	}

}
