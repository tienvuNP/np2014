package Testing;

import isomorph.IsoChecker;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import LTS.State;
import LTS.Transition;
import Main.Main;

public class FinalsTesting {
	
//	@Rule
//	public Timeout globalTimeout = new Timeout(40000); 
	
	@Before
	public void setUp() {
    	HashSet<Transition> temp1 = new HashSet<Transition>();
    	HashMap<String,State> temp2 = new HashMap<String, State>();
        HashSet<Transition> temp3 = new HashSet<Transition>();
        HashSet<String> temp4 = new HashSet<String>();
    	Main.stateSet=temp2;
    	Main.transitionSet=temp3;
    	Main.labelSet=temp4;
    	Main.oldTrans=temp1;

	}		

	@Test
	public void mass() {
		for (int i = 0; i<100 ; i++) {
			setUp();
			A1();setUp();
			AlreadyMinimal();setUp();
			B1();setUp();
			BuggyIntuition();setUp();
			ConcSend_long();setUp();
			Cycle_long();setUp();
			DeadStates();setUp();
			EmptyTransition();setUp();
			ExceptionalState_long();setUp();
			LTSSyntax();setUp();
			ManyDiffTrans_long();setUp();
			ManyStates_long();setUp();
			ManyTaus3();setUp();
			ManyTaus_long();setUp();
			OnlyTau();setUp();
			PseucoConcurrentCounting2();setUp();
			PseucoConcurrentCounting4_long();setUp();
			PseucoMpExample();setUp();
			SimpleCycle();setUp();
			SingleState();setUp();
			SymmetricOne();setUp();
			TauLoop();setUp();
			TausNeeded();setUp();
			Testcase1();setUp();
			Testcase6();setUp();
			TooManyBs();setUp();
			UselessEdge1();setUp();
			UselessEdge2();setUp();
			UselessTauInStartBlock();setUp();
			UselessTauLoop();setUp();
			UselessTransState();setUp();
			WeakGrid();setUp();
			WeakRedundancy();
		}
	}
	@Test
	public void A1() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/A1.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/A1.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void AlreadyMinimal() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/AlreadyMinimal.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/AlreadyMinimal.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}	
	@Test
	public void B1() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/B1.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/B1.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void BuggyIntuition() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/BuggyIntuition.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/BuggyIntuition.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void ConcSend_long() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/ConcSend_long.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/ConcSend_long.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void Cycle_long() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/Cycle_long.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/Cycle_long.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void DeadStates() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/DeadStates.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/DeadStates.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void EmptyTransition() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/EmptyTransition.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/EmptyTransition.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void ExceptionalState_long() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/ExceptionalState_long.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/ExceptionalState_long.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void LTSSyntax() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/LTSSyntax.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/LTSSyntax.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void ManyDiffTrans_long() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyDiffTrans_long.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyDiffTrans_long.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void ManyStates_long() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyStates_long.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyStates_long.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void ManyTaus_long() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyTaus_long.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyTaus_long.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void ManyTaus2_long() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyTaus2_long.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyTaus2_long.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}	
	@Test
	public void ManyTaus3() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyTaus3.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/ManyTaus3.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void OnlyTau() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/OnlyTau.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/OnlyTau.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void PseucoConcurrentCounting2() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/PseucoConcurrentCounting2.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/PseucoConcurrentCounting2.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void PseucoConcurrentCounting4_long() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/PseucoConcurrentCounting4_long.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/PseucoConcurrentCounting4_long.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void PseucoMpExample() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/PseucoMpExample.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/PseucoMpExample.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void SimpleCycle() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/SimpleCycle.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/SimpleCycle.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void SingleState() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/SingleState.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/SingleState.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void SymmetricOne() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/SymmetricOne.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/SymmetricOne.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void TauLoop() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/TauLoop.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/TauLoop.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void TausNeeded() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/TausNeeded.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/TausNeeded.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void Testcase1() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/Testcase1.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/Testcase1.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void Testcase6() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/Testcase6.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/Testcase6.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void TooManyBs() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/TooManyBs.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/TooManyBs.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void UselessEdge1() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessEdge1.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessEdge1.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void UselessEdge2() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessEdge2.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessEdge2.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void UselessTauInStartBlock() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessTauInStartBlock.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessTauInStartBlock.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void UselessTauLoop() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessTauLoop.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessTauLoop.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void UselessTransState() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessTransState.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/UselessTransState.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
	}
	@Test
	public void WeakGrid() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/WeakGrid.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/WeakGrid.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());	
	}
	@Test
	public void WeakRedundancy() {
		String start = null;
		try {
			start = new String(Files.readAllBytes(Paths.get("./testcases/finals/WeakRedundancy.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		String res = null;
		try {
			res = new String(Files.readAllBytes(Paths.get("./testcases/finals/WeakRedundancy.ref.json")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Main.parse(start);
		IsoChecker.assertIsomorphic(start, Main.parseToJSON().toString());
		Main.reach();
		Main.calcWeak();
		Main.minimize();
		IsoChecker.assertIsomorphic(res, Main.parseToJSON().toString());
		
	}
	
	

}
