package Testing;

import java.util.HashMap;
import java.util.HashSet;

import isomorph.IsoChecker;

import javax.json.Json;
import javax.json.JsonObject;

import org.junit.Before;
import org.junit.Test;

import LTS.State;
import LTS.Transition;
import Main.Main;

public class AlgoTesting {
	
	@Before
	public void setUp() {
    	HashSet<Transition> temp1 = new HashSet<Transition>();
    	HashMap<String,State> temp2 = new HashMap<String, State>();
        HashSet<Transition> temp3 = new HashSet<Transition>();
        HashSet<String> temp4 = new HashSet<String>();
    	Main.stateSet=temp2;
    	Main.transitionSet=temp3;
    	Main.labelSet=temp4;
    	Main.oldTrans=temp1;
	}

	@Test
	public void test1() {
		//test object
		JsonObject statesObject = Json.createObjectBuilder()
				.add("1", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.build()).build())
				.add("2", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.add(Json.createObjectBuilder().add("label", "a").add("detailsLabel", false).add("target", "1").build())
						.add(Json.createObjectBuilder().add("label", "a").add("detailsLabel", false).add("target", "3").build())
						.add(Json.createObjectBuilder().add("label", "b").add("detailsLabel", false).add("target", "4").build())
						.build()).build())
				.add("3", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.build()).build())
				.add("4", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.build()).build())
				.build();
		JsonObject ltsObject = Json.createObjectBuilder()
				.add("initialState", "2")
				.add("states", statesObject)
				.build();
		
		
		//result object
		JsonObject resStatesObject = Json.createObjectBuilder()
				.add("0", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.build()).build())
				.add("1", Json.createObjectBuilder().add("transitions", Json.createArrayBuilder()
						.add(Json.createObjectBuilder().add("label", "a").add("detailsLabel", false).add("target", "0").build())
						.add(Json.createObjectBuilder().add("label", "b").add("detailsLabel", false).add("target", "0").build())
						.build()).build())
				.build();
		JsonObject resLtsObject = Json.createObjectBuilder()
				.add("initialState", "1")
				.add("states", resStatesObject)
				.build();
		
		Main.parse(ltsObject.toString());
		Main.minimize();
		IsoChecker.assertIsomorphic(Main.parseToJSON().toString(), resLtsObject.toString());
		
	}
}
